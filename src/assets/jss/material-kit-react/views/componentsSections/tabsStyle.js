import { container } from "assets/jss/material-kit-react.js";

const tabsStyle = {
  section: {
    background: "#153064",
    padding: "70px 0"
  },
  container,
    textCenter: {
    textAlign: "center"
  },
  cardContainer: {
    marginRight: '-15px',
    marginLeft: '-15px',
  }
};

export default tabsStyle;
