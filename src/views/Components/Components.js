import React from "react";

import classNames from "classnames";
import { makeStyles } from "@material-ui/core/styles";
import { Link } from "react-router-dom";
import styles from "assets/jss/material-kit-react/views/components.js";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import userImage from "assets/icons/user.png";
import userImageActive from "assets/icons/userActive.png";
import ListItemIcon from '@material-ui/core/ListItemIcon';

import logoImage from "assets/img/logo/logo.png";

import { useWindowWidth} from '@react-hook/window-size'


import loadable from '@loadable/component'
const MenuLinks = loadable(() => import('components/Menu/MenuLinks'))
const SectionFileTab = loadable(() => import('./Sections/SectionFileTab.js'));
const SectionGeneralTab = loadable(() => import('./Sections/SectionGeneralTab.js'));
const Badge = loadable(() => import('components/Badge/Badge.js'));
const Button = loadable(() => import('components/CustomButtons/Button.js'));
const HeaderLinks = loadable(() => import('components/Header/HeaderLinks.js'));
const Header = loadable(() => import('components/Header/Header.js'));


const useStyles = makeStyles(styles);

export default function Components(props) {
  const classes = useStyles();
  const { ...rest } = props;
  const [opened, setOpened] = React.useState(false)
  const isOpen = (data) => {
      setOpened(!opened);
  }
  const [user, setUser] = React.useState([
    { userName: 'مستخدم 1', watingTime: 'منذ 15 دقيقة', icon: 'not-active' },
    { userName: 'مستخدم 2', watingTime: 'منذ 13 دقيقة', icon: 'not-active' },
    { userName: 'مستخدم 3', watingTime: 'منذ 9 دقيقة', icon: 'not-active' },
    { userName: 'مالك محمد', watingTime: 'منذ 7 دقيقة', icon: 'active' },
    { userName: 'مستخدم 4', watingTime: 'منذ 20 دقيقة', icon: 'not-active' },
    { userName: 'مستخدم 5', watingTime: 'منذ 10 دقيقة', icon: 'not-active' },
    { userName: 'مستخدم 6', watingTime: 'منذ 2 دقيقة', icon: 'not-active' },

  ]);
  const screenSize = useWindowWidth();

  const [mobileState, setMobileState] = React.useState(false);

  const [webState, setWebState] = React.useState(true);

  const toggleDir = ( ) => {
    if (screenSize < 640) {
      setMobileState(true);
      setWebState(false);
    } else {
      setMobileState(false);
      setWebState(true);
    }
  }

  const MobileTab = () => {
    return (
      <>
      <div style={{display: 'flex', textAlign: 'center', justifyContent: 'center'}} >
      <Badge color="transperansy" className={classes.Badge}>١٤</Badge>
      <Button color="primary" transparent="true"  simple className={classes.menuBtn}>قائمة الإنتظار</Button>
        </div>
      </>
    )
  }


  const MobileFileTab = () => {
    return (
      <>
        <div
          color="primary"
          className={classes.fileTab}
          container="true"
        >
          <>
            <Button
              justIcon
              className={classes.bgNotifBtn}
            >
              <i class="far fa-bell fa-2x" style={{ color: 'white' }}></i>
            </Button>

            <Link to={"/login-page"}>
              <Button
                justIcon
                round
                color='gray'
                className={classes.bgUserBtn}
              >{''}
                <i class="far fa-user-circle fa-2x" style={{ color: '#e83866', display: 'inline-flex', justifyContent: 'center'}}></i>
              </Button>
            </Link>
          </>

          <img
            src={logoImage}
            className={classes.logoImg}
            alt="logo"
            
          />


        </div>
      </>
    )
  }


  const MobileGeneralTab = () => {
    return (
      <>
        <div className={classes.genTabContainer}>
          <Button
            endIcon={<div className={classes.styleContainerIconGreen}><i class="fas fa-microphone fa-2x" style={{
              paddingRight: '20%', color: '#1dd950', padding: '15px', borderRadius: '50%', border: '1px success', margin: '0px 5px 0px 5px',
            }}></i></div>}
            color='white'
            className={classes.bgTabBtn}
          >
            {''}
          </Button>

          <Button
            endIcon={<div className={classes.styleContainerIconRed}><i class="fas fa-video fa-2x" style={{ 
              color: '#e21956', padding: '15px', borderRadius: '50%', border: '1px #e21956', margin: '0px 5px 0px 5px'
            }}></i></div>}
            color='white'
            className={classes.bgTabBtn}
          >
              {''}
          </Button>

        <Button
            endIcon={<div className={classes.styleContainerIconYellow}><i class="fas fa-wifi fa-2x" style={{
            color: '#e28519', padding: '15px', borderRadius: '50%', border: '1px #e28519', margin: '0px 5px 0px 5px'
          }}></i></div>}
          color='white'
          className={classes.bgTabBtn}
        >  {''}
          </Button>
          <div className={classes.genTabLabel}>
            <h6 className={classes.genLabel}>سرعة الأنترنت</h6>
            <p className={classes.genLabelSpeed} dir="ltr">٣٦ Mbps</p>
          </div>
        </div>
        
      </>
    )
  }

 
  const MobileMenuTab = () => {
    return (
          <>
                         <List className={classes.usersContainer}>

                         {user.map((user) => (
                              <ListItem button key={user.userName} className={classes.itemContainer}>
                                <ListItem className={classes.userName}  ><h6 className={classes.time}>{user.watingTime}</h6>{user.userName}</ListItem>
                                <ListItemIcon className={classes.bgIcon}>{user.icon === 'active' ? <div style={{}}>
                                  <img src={userImageActive}>
                                  </img></div>
                                  : <div><img src={userImage}>
                                  </img></div>}</ListItemIcon>

                              </ListItem>
                            ))}
                           </List>
           </>

      
    )
  }
  
  return (
    <div className={opened ? classes.menuOpened : classes.menuClosed} onLoad={toggleDir} dir='rtl'>
      <Header
        isMobile={mobileState}
        rightLinks={mobileState === false ? <MenuLinks open={(open) => isOpen(open)} /> :  <MobileMenuTab />  }
        leftLinks={mobileState === false ? <HeaderLinks /> : <MobileTab /> }
        brand={''}
        className={opened === true ? classes.fixedOpen : classes.fixed}
        color="white"
        changeColorOnScroll={{
          height: 400,
          color: "blue"
        }}
       
      />


      <div className={classNames(classes.main, opened ? classes.mainRaised : classes.mainUNRaised )}>
       
        {mobileState === true ? <MobileFileTab /> : ''}
        {mobileState === true ? <MobileGeneralTab /> : ''}
        <SectionFileTab isMobile={mobileState} />
        <SectionGeneralTab isMobile={mobileState} isWeb={webState} isOpen={opened}/>
      </div>
    </div>
  );
}
