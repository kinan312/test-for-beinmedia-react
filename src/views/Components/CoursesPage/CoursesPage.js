import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import styles from "assets/jss/material-kit-react/components/typographyStyle.js";
import { useHistory } from "react-router-dom";
import {courserModel} from "../../Models/CourseModel"
import loadable from '@loadable/component'
const ViewCoursePage = loadable(() => import('../ViewCoursePage/ViewCoursePage'))
const CustomCardList = loadable(() => import('components/CustomCardList/CusromCardList'));
const GridContainer = loadable(() => import('components/Grid/GridContainer.js'));
const GridItem = loadable(() => import('components/Grid/GridItem.js'));
const Button = loadable(() => import('components/CustomButtons/Button.js'));


const useStyles = makeStyles(styles);

export default function CoursesPage({ showArrowIcon , ...props}) {
    
    const classes = useStyles();
    const history = useHistory();
    const [course, setCourse] = React.useState(courserModel)

   

    
    const [showViewCourse, setShowViewCourse] = React.useState(false);
    
    React.useEffect(() => {
        if(props.hideViewCourse){
            setShowViewCourse(true)
        } else {
            setShowViewCourse(false)
        }
    })

   const  changeActiveTab = (course ) => {
    setShowViewCourse(true)
    showArrowIcon(true)
   }
    return (
        <GridContainer justify="space-between"  >
            <GridItem xs={12} sm={12} md={12} className={showViewCourse === false ? classes.cardContainer   : classes.hideCourses }>
                 {props.isMobile ?   <Button className={classes.reserveBtnCourseMobile}>حجز عيادة(20 دينار كويتي)</Button> : ''}
                {course.map((course) => (
                    <CustomCardList isMobile={props.isMobile} courseList={true} containerStyle={'CoursesContainer'} key={course.id} imgSrc={course.imgSrc} courseText={course.courseText} courseSalary={course.courseSalary} coins={course.coins} onClick={() => changeActiveTab(course)}/>
                ))}
            </GridItem>

            <div  className={showViewCourse === true ? classes.showViewCourse : classes.hideViewCourse}>
                <ViewCoursePage  isMobile={props.isMobile} isWeb={props.isWeb} />
            </div>
        </GridContainer>

        
    )
}