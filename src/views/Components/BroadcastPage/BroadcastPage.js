import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import styles from "assets/jss/material-kit-react/components/typographyStyle.js";
import {broadcastModel} from "../../Models/CourseModel"
import loadable from '@loadable/component'
const CustomMobilePlayer = loadable(() => import('components/CustomVideoPlayer/CustomMobilePlayer'))
const Button = loadable(() => import('components/CustomButtons/Button.js'));
const CustomCardList = loadable(() => import('components/CustomCardList/CusromCardList'));
const GridContainer = loadable(() => import('components/Grid/GridContainer.js'));
const CustomPlayer = loadable(() => import('components/CustomVideoPlayer/CustomPlayer'));
const GridItem = loadable(() => import('components/Grid/GridItem.js'));



const useStyles = makeStyles(styles);

export default function BroadcastPage(props) {
    const classes = useStyles();

    const [course, setCourse] = React.useState(broadcastModel)


    const MobilePlayer = () => {
        if (props.isMobile === true) {
            return (

                <GridItem xs={12} sm={12} md={7} >
                    <CustomMobilePlayer url='' isMobile={props.isMobile} />
                </GridItem>

            )
        }
    }

    const WebPlayer = () => {
        if (props.isMobile === false) {
            return (

                <GridItem xs={12} sm={12} md={7} >
                    <CustomPlayer url='' isWeb={props.isWeb} />
                </GridItem>

            )
        }
    }


    return (
        <GridContainer justify="space-between"  >

        {MobilePlayer()}

            <GridItem xs={12} sm={12} md={5} className={props.isMobile ? classes.mobileCardContainer : classes.cardContainer}>
                {props.isMobile ?   <Button className={classes.reserveBtnCardMobile}>حجز عيادة(20 دينار كويتي)</Button> : ''}
                {course.map((course) => (
                    <CustomCardList courseList={false} key={course.id} imgSrc={course.imgSrc} courseText={course.courseText} courseSalary={course.courseSalary} />
                ))}


            </GridItem>

                 
          {WebPlayer()}
                  
        </GridContainer>
    )
}